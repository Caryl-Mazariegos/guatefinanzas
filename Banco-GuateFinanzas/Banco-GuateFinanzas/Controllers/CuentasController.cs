﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CuentasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Cuentas
        public async Task<ActionResult> Index()
        {
            var cuentas = db.Cuentas.Include(c => c.Persona).Include(c => c.TipoCuenta);
            return View(await cuentas.ToListAsync());
        }

        // GET: Cuentas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = await db.Cuentas.FindAsync(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        // GET: Cuentas/Create
        public ActionResult Create()
        {
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi");
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo");
            return View();
        }

        // POST: Cuentas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PersonaID,TipoCuentaID,Numero,Saldo")] Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Cuentas.Add(cuenta);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", cuenta.PersonaID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        // GET: Cuentas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = await db.Cuentas.FindAsync(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", cuenta.PersonaID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        // POST: Cuentas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PersonaID,TipoCuentaID,Numero,Saldo")] Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuenta).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", cuenta.PersonaID);
            ViewBag.TipoCuentaID = new SelectList(db.TipoCuentas, "ID", "Tipo", cuenta.TipoCuentaID);
            return View(cuenta);
        }

        // GET: Cuentas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuenta cuenta = await db.Cuentas.FindAsync(id);
            if (cuenta == null)
            {
                return HttpNotFound();
            }
            return View(cuenta);
        }

        // POST: Cuentas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Cuenta cuenta = await db.Cuentas.FindAsync(id);
            db.Cuentas.Remove(cuenta);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
