﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TipoTarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoTarjetas
        public async Task<ActionResult> Index()
        {
            return View(await db.TipoTarjetas.ToListAsync());
        }

        // GET: TipoTarjetas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = await db.TipoTarjetas.FindAsync(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoTarjetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Tipo")] TipoTarjeta tipoTarjeta)
        {
            if (ModelState.IsValid)
            {
                db.TipoTarjetas.Add(tipoTarjeta);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = await db.TipoTarjetas.FindAsync(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // POST: TipoTarjetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Tipo")] TipoTarjeta tipoTarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoTarjeta).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = await db.TipoTarjetas.FindAsync(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // POST: TipoTarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TipoTarjeta tipoTarjeta = await db.TipoTarjetas.FindAsync(id);
            db.TipoTarjetas.Remove(tipoTarjeta);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
