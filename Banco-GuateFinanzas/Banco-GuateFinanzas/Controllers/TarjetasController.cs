﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tarjetas
        public async Task<ActionResult> Index()
        {
            var tarjetas = db.Tarjetas.Include(t => t.Cuenta).Include(t => t.TBloqueo).Include(t => t.TipoTarjeta);
            return View(await tarjetas.ToListAsync());
        }

        // GET: Tarjetas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = await db.Tarjetas.FindAsync(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // GET: Tarjetas/Create
        public ActionResult Create()
        {
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero");
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo");
            ViewBag.TipoTarjetaID = new SelectList(db.TipoTarjetas, "ID", "Tipo");
            return View();
        }

        // POST: Tarjetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,CuentaID,TipoTarjetaID,TBloqueoID,Nro,Pin,Vence,Limite,FechaPago")] Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Tarjetas.Add(tarjeta);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", tarjeta.CuentaID);
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", tarjeta.TBloqueoID);
            ViewBag.TipoTarjetaID = new SelectList(db.TipoTarjetas, "ID", "Tipo", tarjeta.TipoTarjetaID);
            return View(tarjeta);
        }

        // GET: Tarjetas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = await db.Tarjetas.FindAsync(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", tarjeta.CuentaID);
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", tarjeta.TBloqueoID);
            ViewBag.TipoTarjetaID = new SelectList(db.TipoTarjetas, "ID", "Tipo", tarjeta.TipoTarjetaID);
            return View(tarjeta);
        }

        // POST: Tarjetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,CuentaID,TipoTarjetaID,TBloqueoID,Nro,Pin,Vence,Limite,FechaPago")] Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tarjeta).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", tarjeta.CuentaID);
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", tarjeta.TBloqueoID);
            ViewBag.TipoTarjetaID = new SelectList(db.TipoTarjetas, "ID", "Tipo", tarjeta.TipoTarjetaID);
            return View(tarjeta);
        }

        // GET: Tarjetas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = await db.Tarjetas.FindAsync(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // POST: Tarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Tarjeta tarjeta = await db.Tarjetas.FindAsync(id);
            db.Tarjetas.Remove(tarjeta);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
