﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TransaccionesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Transacciones
        public async Task<ActionResult> Index()
        {
            var transacciones = db.Transacciones.Include(t => t.Cuenta);
            return View(await transacciones.ToListAsync());
        }

        // GET: Transacciones/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transacciones transacciones = await db.Transacciones.FindAsync(id);
            if (transacciones == null)
            {
                return HttpNotFound();
            }
            return View(transacciones);
        }

        // GET: Transacciones/Create
        public ActionResult Create()
        {
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero");
            return View();
        }

        // POST: Transacciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,CuentaID,Lugar,Fecha,SaldoD,SaldoR")] Transacciones transacciones)
        {
            if (ModelState.IsValid)
            {
                db.Transacciones.Add(transacciones);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", transacciones.CuentaID);
            return View(transacciones);
        }

        // GET: Transacciones/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transacciones transacciones = await db.Transacciones.FindAsync(id);
            if (transacciones == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", transacciones.CuentaID);
            return View(transacciones);
        }

        // POST: Transacciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,CuentaID,Lugar,Fecha,SaldoD,SaldoR")] Transacciones transacciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transacciones).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaID = new SelectList(db.Cuentas, "ID", "Numero", transacciones.CuentaID);
            return View(transacciones);
        }

        // GET: Transacciones/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transacciones transacciones = await db.Transacciones.FindAsync(id);
            if (transacciones == null)
            {
                return HttpNotFound();
            }
            return View(transacciones);
        }

        // POST: Transacciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Transacciones transacciones = await db.Transacciones.FindAsync(id);
            db.Transacciones.Remove(transacciones);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
