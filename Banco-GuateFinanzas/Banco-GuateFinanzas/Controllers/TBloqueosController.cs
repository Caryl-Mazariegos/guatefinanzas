﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TBloqueosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TBloqueos
        public async Task<ActionResult> Index()
        {
            return View(await db.TBloqueos.ToListAsync());
        }

        // GET: TBloqueos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBloqueo tBloqueo = await db.TBloqueos.FindAsync(id);
            if (tBloqueo == null)
            {
                return HttpNotFound();
            }
            return View(tBloqueo);
        }

        // GET: TBloqueos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TBloqueos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Tipo")] TBloqueo tBloqueo)
        {
            if (ModelState.IsValid)
            {
                db.TBloqueos.Add(tBloqueo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tBloqueo);
        }

        // GET: TBloqueos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBloqueo tBloqueo = await db.TBloqueos.FindAsync(id);
            if (tBloqueo == null)
            {
                return HttpNotFound();
            }
            return View(tBloqueo);
        }

        // POST: TBloqueos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Tipo")] TBloqueo tBloqueo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBloqueo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tBloqueo);
        }

        // GET: TBloqueos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBloqueo tBloqueo = await db.TBloqueos.FindAsync(id);
            if (tBloqueo == null)
            {
                return HttpNotFound();
            }
            return View(tBloqueo);
        }

        // POST: TBloqueos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TBloqueo tBloqueo = await db.TBloqueos.FindAsync(id);
            db.TBloqueos.Remove(tBloqueo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
