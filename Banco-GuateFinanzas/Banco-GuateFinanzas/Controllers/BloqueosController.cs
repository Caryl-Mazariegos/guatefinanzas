﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    public class BloqueosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Bloqueos
        public async Task<ActionResult> Index()
        {
            var bloqueos = db.Bloqueos.Include(b => b.TBloqueo);
            return View(await bloqueos.ToListAsync());
        }

        // GET: Bloqueos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bloqueo bloqueo = await db.Bloqueos.FindAsync(id);
            if (bloqueo == null)
            {
                return HttpNotFound();
            }
            return View(bloqueo);
        }

        // GET: Bloqueos/Create
        public ActionResult Create()
        {
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo");
            return View();
        }

        // POST: Bloqueos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,TBloqueoID")] Bloqueo bloqueo)
        {
            if (ModelState.IsValid)
            {
                db.Bloqueos.Add(bloqueo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", bloqueo.TBloqueoID);
            return View(bloqueo);
        }

        // GET: Bloqueos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bloqueo bloqueo = await db.Bloqueos.FindAsync(id);
            if (bloqueo == null)
            {
                return HttpNotFound();
            }
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", bloqueo.TBloqueoID);
            return View(bloqueo);
        }

        // POST: Bloqueos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,TBloqueoID")] Bloqueo bloqueo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bloqueo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TBloqueoID = new SelectList(db.TBloqueos, "ID", "Tipo", bloqueo.TBloqueoID);
            return View(bloqueo);
        }

        // GET: Bloqueos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bloqueo bloqueo = await db.Bloqueos.FindAsync(id);
            if (bloqueo == null)
            {
                return HttpNotFound();
            }
            return View(bloqueo);
        }

        // POST: Bloqueos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Bloqueo bloqueo = await db.Bloqueos.FindAsync(id);
            db.Bloqueos.Remove(bloqueo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
