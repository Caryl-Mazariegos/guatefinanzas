﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco_GuateFinanzas.Models;

namespace Banco_GuateFinanzas.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PrestamoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Prestamoes
        public async Task<ActionResult> Index()
        {
            var prestamoes = db.Prestamoes.Include(p => p.Persona);
            return View(await prestamoes.ToListAsync());
        }

        // GET: Prestamoes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestamo prestamo = await db.Prestamoes.FindAsync(id);
            if (prestamo == null)
            {
                return HttpNotFound();
            }
            return View(prestamo);
        }

        // GET: Prestamoes/Create
        public ActionResult Create()
        {
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi");
            return View();
        }

        // POST: Prestamoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,PersonaID,Meses,CMeses,Monto,FechaI,FechaP")] Prestamo prestamo)
        {
            if (ModelState.IsValid)
            {
                db.Prestamoes.Add(prestamo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", prestamo.PersonaID);
            return View(prestamo);
        }

        // GET: Prestamoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestamo prestamo = await db.Prestamoes.FindAsync(id);
            if (prestamo == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", prestamo.PersonaID);
            return View(prestamo);
        }

        // POST: Prestamoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,PersonaID,Meses,CMeses,Monto,FechaI,FechaP")] Prestamo prestamo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prestamo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PersonaID = new SelectList(db.Personas, "ID", "Dpi", prestamo.PersonaID);
            return View(prestamo);
        }

        // GET: Prestamoes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prestamo prestamo = await db.Prestamoes.FindAsync(id);
            if (prestamo == null)
            {
                return HttpNotFound();
            }
            return View(prestamo);
        }

        // POST: Prestamoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Prestamo prestamo = await db.Prestamoes.FindAsync(id);
            db.Prestamoes.Remove(prestamo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
