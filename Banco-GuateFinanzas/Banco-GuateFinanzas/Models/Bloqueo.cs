﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class Bloqueo
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Bloqueo/Desbloqueo")]
        public int TBloqueoID { get; set; }
        public TBloqueo TBloqueo { get; set; }
    }
}