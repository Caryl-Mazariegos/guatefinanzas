﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class Tarjeta
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Cuenta")]
        public int CuentaID { get; set; }
        public Cuenta Cuenta { get; set; }

        [Required]
        [Display(Name = "Tipo de tarjeta")]
        public int TipoTarjetaID { get; set; }
        public TipoTarjeta TipoTarjeta { get; set; }

        [Required]
        [Display(Name = "Estado")]
        public int TBloqueoID { get; set; }
        public TBloqueo TBloqueo { get; set; }

        [Required]
        [Display(Name = "Numero de tarjeta")]
        [StringLength(8)]
        [Range(1, 1000000000)]
        public String Nro { get; set; }

        [Required]
        [Display(Name = "Pin")]
        [StringLength(4)]
        [Range(1, 1000000000)]
        [DataType(DataType.Password)]
        public String Pin { get; set; }

        [Required]
        [Display(Name = "Fecha de vencimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Vence { get; set; }


        [Required]
        [Display(Name = "Límite de la tarjeta")]
        public int Limite { get; set; }

        [Required]
        [Display(Name = "Fecha de pago")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaPago { get; set; }

    }
}