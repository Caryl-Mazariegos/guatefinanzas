﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class TipoCuenta 
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public String Tipo { get; set; }
    }

}