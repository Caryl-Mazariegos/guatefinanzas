﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class TBloqueo
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Bloqueo/Desbloqueo")]
        public string Tipo { get; set; }
    }
}