﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class Transacciones
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Cuenta")]
        public int CuentaID { get; set; }
        public Cuenta Cuenta { get; set; }

        [Required]
        [Display(Name = "Lugar en donde se realizo la transferencia")]
        public string Lugar { get; set; }

        [Required]
        [Display(Name = "Fecha de Transferencia")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Required]
        [Display(Name = "Saldo Disponible")]
        public Decimal SaldoD { get; set; }

        [Required]
        [Display(Name = "Saldo Retirado")]
        public Decimal SaldoR { get; set; }
    }
}