﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco_GuateFinanzas.Models
{
    public class Prestamo
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Cuenta")]
        public int PersonaID { get; set; }
        public Persona Persona { get; set; }

        [Required]
        [Display(Name = "Cantidad de Meses")]
        public int CMeses { get; set; }

        [Required]
        [Display(Name = "Monto")]
        public int Monto { get; set; }

        [Required]
        [Display(Name = "Fecha de Inicio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaI { get; set; }

        [Required]
        [Display(Name = "Fecha de Pago")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaP { get; set; }

    }
}