﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Banco_GuateFinanzas.Models
{
    public class Persona
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Ingrese el numero de identificación")]
        [StringLength(13)]
        public String Dpi { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String Nombre { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String Apellido { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String Direccion { get; set; }

        [Required]
        public int Telefono { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        [Display(Name = "Correo electrónico")]

        public String Correo { get; set; }

        [DisplayName("Nombre referencia personal")]
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String NombreRef { get; set; }

        [Required]
        [DisplayName("Numero telefonico de la referencia personal")]
        public int TelefonoRef { get; set; }

        [DisplayName("Nombre referencia personal")]
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String NombreRef1 { get; set; }

        [Required]
        [DisplayName("Numero telefonico de la referencia personal")]
        public int TelefonoRef1 { get; set; }

        [DisplayName("Nombre referencia personal")]
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String NombreRef2 { get; set; }

        [Required]
        [DisplayName("Numero telefonico de la referencia personal")]
        public int TelefonoRef2 { get; set; }


    }
}