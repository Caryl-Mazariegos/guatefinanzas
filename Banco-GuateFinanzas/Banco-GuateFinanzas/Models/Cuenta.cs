﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Banco_GuateFinanzas.Models
{
    public class Cuenta 
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Persona")]
        public int PersonaID { get; set; }
        public Persona Persona { get; set; }

        [Required]
        [Display(Name = "Tipo de Cuenta")]
        public int TipoCuentaID { get; set; }
        public TipoCuenta TipoCuenta { get; set; }


        [Display(Name = "Numero de cuenta")]
        public String Numero { get; set; }

        [Required]
        [Display(Name = "Saldo de cuenta")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Saldo invalido use dos decimales")]
        public decimal Saldo { get; set; }



    }
}