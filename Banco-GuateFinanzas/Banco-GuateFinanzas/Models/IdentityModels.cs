﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Banco_GuateFinanzas.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Bloqueo> Bloqueos { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.TBloqueo> TBloqueos { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Cuenta> Cuentas { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Persona> Personas { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.TipoCuenta> TipoCuentas { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Prestamo> Prestamoes { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Tarjeta> Tarjetas { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.TipoTarjeta> TipoTarjetas { get; set; }

        public System.Data.Entity.DbSet<Banco_GuateFinanzas.Models.Transacciones> Transacciones { get; set; }
    }
}