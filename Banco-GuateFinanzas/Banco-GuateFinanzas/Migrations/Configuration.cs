namespace Banco_GuateFinanzas.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
using Banco_GuateFinanzas.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Banco_GuateFinanzas.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Banco_GuateFinanzas.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            

     if (!context.Users.Any(u => u.UserName == "Caryl3312"))
            {
                var roleSore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleSore);

                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "Caryl3312" };

                manager.Create(user, "kinal2015");
                var roleAdmin = new IdentityRole { Name = "Admin" };
                roleManager.Create(roleAdmin);
                roleManager.Create(new IdentityRole { Name = "User" });

                manager.AddToRole(user.Id, "Admin");

        }
    }
}
}
