namespace Banco_GuateFinanzas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bloqueos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TBloqueoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TBloqueos", t => t.TBloqueoID, cascadeDelete: true)
                .Index(t => t.TBloqueoID);
            
            CreateTable(
                "dbo.TBloqueos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Tipo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Cuentas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PersonaID = c.Int(nullable: false),
                        TipoCuentaID = c.Int(nullable: false),
                        Numero = c.String(),
                        Saldo = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Personas", t => t.PersonaID, cascadeDelete: true)
                .ForeignKey("dbo.TipoCuentas", t => t.TipoCuentaID, cascadeDelete: true)
                .Index(t => t.PersonaID)
                .Index(t => t.TipoCuentaID);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Dpi = c.String(nullable: false, maxLength: 13),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Apellido = c.String(nullable: false, maxLength: 50),
                        Direccion = c.String(nullable: false, maxLength: 50),
                        Telefono = c.Int(nullable: false),
                        Correo = c.String(nullable: false),
                        NombreRef = c.String(nullable: false, maxLength: 50),
                        TelefonoRef = c.Int(nullable: false),
                        NombreRef1 = c.String(nullable: false, maxLength: 50),
                        TelefonoRef1 = c.Int(nullable: false),
                        NombreRef2 = c.String(nullable: false, maxLength: 50),
                        TelefonoRef2 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TipoCuentas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Tipo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Prestamoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PersonaID = c.Int(nullable: false),
                        CMeses = c.Int(nullable: false),
                        Monto = c.Int(nullable: false),
                        FechaI = c.DateTime(nullable: false),
                        FechaP = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Personas", t => t.PersonaID, cascadeDelete: true)
                .Index(t => t.PersonaID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tarjetas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CuentaID = c.Int(nullable: false),
                        TipoTarjetaID = c.Int(nullable: false),
                        TBloqueoID = c.Int(nullable: false),
                        Nro = c.String(nullable: false, maxLength: 8),
                        Pin = c.String(nullable: false, maxLength: 4),
                        Vence = c.DateTime(nullable: false),
                        Limite = c.Int(nullable: false),
                        FechaPago = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cuentas", t => t.CuentaID, cascadeDelete: true)
                .ForeignKey("dbo.TBloqueos", t => t.TBloqueoID, cascadeDelete: true)
                .ForeignKey("dbo.TipoTarjetas", t => t.TipoTarjetaID, cascadeDelete: true)
                .Index(t => t.CuentaID)
                .Index(t => t.TBloqueoID)
                .Index(t => t.TipoTarjetaID);
            
            CreateTable(
                "dbo.TipoTarjetas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Tipo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Transacciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CuentaID = c.Int(nullable: false),
                        Lugar = c.String(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        SaldoD = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaldoR = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cuentas", t => t.CuentaID, cascadeDelete: true)
                .Index(t => t.CuentaID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserName = c.String(),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserClaims", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transacciones", "CuentaID", "dbo.Cuentas");
            DropForeignKey("dbo.Tarjetas", "TipoTarjetaID", "dbo.TipoTarjetas");
            DropForeignKey("dbo.Tarjetas", "TBloqueoID", "dbo.TBloqueos");
            DropForeignKey("dbo.Tarjetas", "CuentaID", "dbo.Cuentas");
            DropForeignKey("dbo.Prestamoes", "PersonaID", "dbo.Personas");
            DropForeignKey("dbo.Cuentas", "TipoCuentaID", "dbo.TipoCuentas");
            DropForeignKey("dbo.Cuentas", "PersonaID", "dbo.Personas");
            DropForeignKey("dbo.Bloqueos", "TBloqueoID", "dbo.TBloqueos");
            DropIndex("dbo.AspNetUserClaims", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.Transacciones", new[] { "CuentaID" });
            DropIndex("dbo.Tarjetas", new[] { "TipoTarjetaID" });
            DropIndex("dbo.Tarjetas", new[] { "TBloqueoID" });
            DropIndex("dbo.Tarjetas", new[] { "CuentaID" });
            DropIndex("dbo.Prestamoes", new[] { "PersonaID" });
            DropIndex("dbo.Cuentas", new[] { "TipoCuentaID" });
            DropIndex("dbo.Cuentas", new[] { "PersonaID" });
            DropIndex("dbo.Bloqueos", new[] { "TBloqueoID" });
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Transacciones");
            DropTable("dbo.TipoTarjetas");
            DropTable("dbo.Tarjetas");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Prestamoes");
            DropTable("dbo.TipoCuentas");
            DropTable("dbo.Personas");
            DropTable("dbo.Cuentas");
            DropTable("dbo.TBloqueos");
            DropTable("dbo.Bloqueos");
        }
    }
}
