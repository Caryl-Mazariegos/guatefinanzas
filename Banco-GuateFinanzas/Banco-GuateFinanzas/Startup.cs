﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Banco_GuateFinanzas.Startup))]
namespace Banco_GuateFinanzas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
